namespace Identity.Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolesAndStuff : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "api.Applications",
            //    c => new
            //        {
            //            PrimaryKey = c.Guid(nullable: false),
            //            Name = c.String(nullable: false),
            //            Description = c.String(maxLength: 1000),
            //            LogoUrl = c.String(),
            //            Audience = c.String(nullable: false),
            //            Issuer = c.String(nullable: false),
            //            AccessTokenLifetime = c.Int(nullable: false),
            //            RefreshTokenLifetime = c.Int(nullable: false),
            //            AllowRefreshToken = c.Boolean(nullable: false),
            //            SigningKey = c.Binary(),
            //            Id = c.String(nullable: false),
            //            Slug = c.String(),
            //            Weight = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateModified = c.DateTime(nullable: false),
            //            CreatedBy = c.String(nullable: false),
            //            ModifiedBy = c.String(nullable: false),
            //            TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //            IsRemoved = c.Boolean(nullable: false),
            //            Enabled = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.PrimaryKey);
            
            //CreateTable(
            //    "api.Clients",
            //    c => new
            //        {
            //            PrimaryKey = c.Guid(nullable: false),
            //            ClientId = c.String(nullable: false, maxLength: 250),
            //            Name = c.String(nullable: false),
            //            ClientSecret = c.String(),
            //            AccessTokenLifetime = c.Int(nullable: false),
            //            AllowRefreshToken = c.Boolean(nullable: false),
            //            RefreshTokenLifetime = c.Int(nullable: false),
            //            HasClientSecret = c.Boolean(nullable: false),
            //            Id = c.String(nullable: false),
            //            Slug = c.String(),
            //            Weight = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateModified = c.DateTime(nullable: false),
            //            CreatedBy = c.String(nullable: false),
            //            ModifiedBy = c.String(nullable: false),
            //            TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //            IsRemoved = c.Boolean(nullable: false),
            //            Enabled = c.Boolean(nullable: false),
            //            Application_PrimaryKey = c.Guid(),
            //            Role_PrimaryKey = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.PrimaryKey)
            //    .ForeignKey("api.Applications", t => t.Application_PrimaryKey)
            //    .ForeignKey("api.Roles", t => t.Role_PrimaryKey)
            //    .Index(t => t.Application_PrimaryKey)
            //    .Index(t => t.Role_PrimaryKey);
            
            //CreateTable(
            //    "api.Scopes",
            //    c => new
            //        {
            //            PrimaryKey = c.Guid(nullable: false),
            //            Name = c.String(nullable: false, maxLength: 100),
            //            Description = c.String(nullable: false, maxLength: 500),
            //            Id = c.String(nullable: false),
            //            Slug = c.String(),
            //            Weight = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateModified = c.DateTime(nullable: false),
            //            CreatedBy = c.String(nullable: false),
            //            ModifiedBy = c.String(nullable: false),
            //            TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //            IsRemoved = c.Boolean(nullable: false),
            //            Enabled = c.Boolean(nullable: false),
            //            Application_PrimaryKey = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.PrimaryKey)
            //    .ForeignKey("api.Applications", t => t.Application_PrimaryKey)
            //    .Index(t => t.Application_PrimaryKey);
            
            //CreateTable(
            //    "api.Users",
            //    c => new
            //        {
            //            PrimaryKey = c.Guid(nullable: false),
            //            Username = c.String(nullable: false, maxLength: 100),
            //            FirstName = c.String(maxLength: 100),
            //            LastName = c.String(maxLength: 100),
            //            EmailAddress = c.String(nullable: false, maxLength: 256),
            //            EmailVerificationToken = c.String(maxLength: 100),
            //            PasswordVerificationToken = c.String(maxLength: 100),
            //            PasswordVerificationTokenExpirationDate = c.DateTime(),
            //            LastPasswordChangedDate = c.DateTime(),
            //            FailedPasswordAttemptCount = c.Int(nullable: false),
            //            MustChangePasswordOnLogin = c.Boolean(nullable: false),
            //            FailedPasswordAttemptWindowStart = c.DateTime(),
            //            PasswordHash = c.String(maxLength: 100),
            //            IsEmailVerified = c.Boolean(nullable: false),
            //            Id = c.String(nullable: false),
            //            Slug = c.String(),
            //            Weight = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateModified = c.DateTime(nullable: false),
            //            CreatedBy = c.String(nullable: false),
            //            ModifiedBy = c.String(nullable: false),
            //            TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //            IsRemoved = c.Boolean(nullable: false),
            //            Enabled = c.Boolean(nullable: false),
            //            Application_PrimaryKey = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.PrimaryKey)
            //    .ForeignKey("api.Applications", t => t.Application_PrimaryKey)
            //    .Index(t => t.Application_PrimaryKey);
            
            //CreateTable(
            //    "api.Claims",
            //    c => new
            //        {
            //            PrimaryKey = c.Guid(nullable: false),
            //            ClaimType = c.String(nullable: false),
            //            Resource = c.String(nullable: false),
            //            Right = c.String(nullable: false),
            //            Id = c.String(nullable: false),
            //            Slug = c.String(),
            //            Weight = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateModified = c.DateTime(nullable: false),
            //            CreatedBy = c.String(nullable: false),
            //            ModifiedBy = c.String(nullable: false),
            //            TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //            IsRemoved = c.Boolean(nullable: false),
            //            Enabled = c.Boolean(nullable: false),
            //            Role_PrimaryKey = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.PrimaryKey)
            //    .ForeignKey("api.Roles", t => t.Role_PrimaryKey)
            //    .Index(t => t.Role_PrimaryKey);
            
            //CreateTable(
            //    "api.Roles",
            //    c => new
            //        {
            //            PrimaryKey = c.Guid(nullable: false),
            //            Name = c.String(nullable: false, maxLength: 100),
            //            Description = c.String(nullable: false, maxLength: 500),
            //            Id = c.String(nullable: false),
            //            Slug = c.String(),
            //            Weight = c.Int(nullable: false),
            //            DateCreated = c.DateTime(nullable: false),
            //            DateModified = c.DateTime(nullable: false),
            //            CreatedBy = c.String(nullable: false),
            //            ModifiedBy = c.String(nullable: false),
            //            TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
            //            IsRemoved = c.Boolean(nullable: false),
            //            Enabled = c.Boolean(nullable: false),
            //            Application_PrimaryKey = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.PrimaryKey)
            //    .ForeignKey("api.Applications", t => t.Application_PrimaryKey)
            //    .Index(t => t.Application_PrimaryKey);
            
            //CreateTable(
            //    "api.Client_Scopes",
            //    c => new
            //        {
            //            Client_PrimaryKey = c.Guid(nullable: false),
            //            Scope_PrimaryKey = c.Guid(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.Client_PrimaryKey, t.Scope_PrimaryKey })
            //    .ForeignKey("api.Clients", t => t.Client_PrimaryKey, cascadeDelete: true)
            //    .ForeignKey("api.Scopes", t => t.Scope_PrimaryKey, cascadeDelete: true)
            //    .Index(t => t.Client_PrimaryKey)
            //    .Index(t => t.Scope_PrimaryKey);
            
            //CreateTable(
            //    "api.User_Claims",
            //    c => new
            //        {
            //            User_PrimaryKey = c.Guid(nullable: false),
            //            Claim_PrimaryKey = c.Guid(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.User_PrimaryKey, t.Claim_PrimaryKey })
            //    .ForeignKey("api.Users", t => t.User_PrimaryKey, cascadeDelete: true)
            //    .ForeignKey("api.Claims", t => t.Claim_PrimaryKey, cascadeDelete: true)
            //    .Index(t => t.User_PrimaryKey)
            //    .Index(t => t.Claim_PrimaryKey);
            
            CreateTable(
                "api.Users_Roles",
                c => new
                    {
                        Role_PrimaryKey = c.Guid(nullable: false),
                        User_PrimaryKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_PrimaryKey, t.User_PrimaryKey })
                .ForeignKey("api.Roles", t => t.Role_PrimaryKey, cascadeDelete: true)
                .ForeignKey("api.Users", t => t.User_PrimaryKey, cascadeDelete: true)
                .Index(t => t.Role_PrimaryKey)
                .Index(t => t.User_PrimaryKey);
            
        }
        
        public override void Down()
        {
            DropForeignKey("api.Users", "Application_PrimaryKey", "api.Applications");
            DropForeignKey("api.RoleUsers", "User_PrimaryKey", "api.Users");
            DropForeignKey("api.RoleUsers", "Role_PrimaryKey", "api.Roles");
            DropForeignKey("api.Claims", "Role_PrimaryKey", "api.Roles");
            DropForeignKey("api.Roles", "Application_PrimaryKey", "api.Applications");
            DropForeignKey("api.Clients", "Role_PrimaryKey", "api.Roles");
            DropForeignKey("api.User_Claims", "Claim_PrimaryKey", "api.Claims");
            DropForeignKey("api.User_Claims", "User_PrimaryKey", "api.Users");
            DropForeignKey("api.Client_Scopes", "Scope_PrimaryKey", "api.Scopes");
            DropForeignKey("api.Client_Scopes", "Client_PrimaryKey", "api.Clients");
            DropForeignKey("api.Scopes", "Application_PrimaryKey", "api.Applications");
            DropForeignKey("api.Clients", "Application_PrimaryKey", "api.Applications");
            DropIndex("api.RoleUsers", new[] { "User_PrimaryKey" });
            DropIndex("api.RoleUsers", new[] { "Role_PrimaryKey" });
            DropIndex("api.User_Claims", new[] { "Claim_PrimaryKey" });
            DropIndex("api.User_Claims", new[] { "User_PrimaryKey" });
            DropIndex("api.Client_Scopes", new[] { "Scope_PrimaryKey" });
            DropIndex("api.Client_Scopes", new[] { "Client_PrimaryKey" });
            DropIndex("api.Roles", new[] { "Application_PrimaryKey" });
            DropIndex("api.Claims", new[] { "Role_PrimaryKey" });
            DropIndex("api.Users", new[] { "Application_PrimaryKey" });
            DropIndex("api.Scopes", new[] { "Application_PrimaryKey" });
            DropIndex("api.Clients", new[] { "Role_PrimaryKey" });
            DropIndex("api.Clients", new[] { "Application_PrimaryKey" });
            DropTable("api.RoleUsers");
            DropTable("api.User_Claims");
            DropTable("api.Client_Scopes");
            DropTable("api.Roles");
            DropTable("api.Claims");
            DropTable("api.Users");
            DropTable("api.Scopes");
            DropTable("api.Clients");
            DropTable("api.Applications");
        }
    }
}
