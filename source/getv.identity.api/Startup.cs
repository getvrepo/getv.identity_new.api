﻿using Foundation.Core;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;

[assembly: OwinStartup(typeof(Identity.Api.Startup))]
namespace Identity.Api
{
	public class Startup
	{
	    public void Configuration(IAppBuilder app)
	    {
	        PasswordHasher<JhmHmacSha1>.Initialize();
	        PasswordHasher<GetvSha256>.Initialize();
	        PasswordHasher<Pbkdf2>.Initialize();

	        IdentityContext.Initialize();



            //var tokenCorsPolicy = new CorsPolicy
            //{
            //    AllowAnyOrigin = true,
            //    AllowAnyHeader = true,
            //    AllowAnyMethod = true
            //};

            //var corsOptions = new CorsOptions
            //{
            //    PolicyProvider = new CorsPolicyProvider()
            //    {
            //        PolicyResolver =
            //            request => Task.FromResult(request.Path.ToString().StartsWith("/token") ? tokenCorsPolicy : null)
            //    }
            //};

            //app.UseCors(corsOptions);

            app.UseCors(CorsOptions.AllowAll);

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                // change this for production
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                Provider = new GetvAuthorizationServerProvider(),
                RefreshTokenProvider = new GetvRefreshTokenProvider(),
                AccessTokenProvider = new GetvAccessTokenProvider(),
            });

	        
			app.UseWebApi(WebApiConfig.Register());
		}
	}
}