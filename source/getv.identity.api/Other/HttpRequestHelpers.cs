﻿using System.Net.Http;

namespace Foundation.WebApi
{
    public class HttpRequestHelpers
    {
        public static string GetJSONFromRequestBody(HttpRequestMessage request)
        {
            var contentType = request.Content.Headers.ContentType;
            var contentInString = request.Content.ReadAsStringAsync().Result;
            request.Content = new StringContent(contentInString);
            request.Content.Headers.ContentType = contentType;
            return contentInString;
        }
    }

}