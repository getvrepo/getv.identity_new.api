﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace Foundation.Core
{
	public static class SequentialGuid
	{
		[DllImport("rpcrt4.dll", SetLastError = true)]
		static extern int UuidCreateSequential(out Guid guid);

		public static Func<Guid> NewGuid { get; private set; }

		static SequentialGuid()
		{
			Reset();
		}

		static void Reset()
		{
			NewGuid = () =>
			{
				Guid guid;
				var retVal = UuidCreateSequential(out guid);
				if (retVal == 0) return guid;
				throw new Exception("UuidCreateSequential failed: " + retVal);
			};
		}

	}
}