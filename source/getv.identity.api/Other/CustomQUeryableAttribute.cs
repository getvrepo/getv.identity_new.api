﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.OData.Query;


namespace Foundation.WebApi
{
    public class CustomQueryableAttribute : QueryableAttribute
    {

        public CustomQueryableAttribute()
        {
        }

        /// <summary>
        /// Occurs before the action method is invoked.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            // disqualify respnses such as XML Responses
            if ((actionExecutedContext == null) || (actionExecutedContext.Response == null) ||
                (actionExecutedContext.Response.Content == null))
            {
                return;
            }
            var query =
                ((System.Net.Http.ObjectContent) (actionExecutedContext.Response.Content)).Value as IQueryable<object>;
            if (query != null && !actionExecutedContext.Request.GetTotalItemCount().HasValue)
            {
                var totalCount = query.Count();
                actionExecutedContext.Request.SetTotalItemCount(totalCount);
            }

            //var parameters = actionExecutedContext.Request.RequestUri.ParseQueryString();
            //var envelopeParameter = parameters["envelope"];
            //var useEnvelope = true;
            //var envelopeSuccess = Boolean.TryParse(envelopeParameter, out useEnvelope);
            //actionExecutedContext.Request.SetUseApiEnvelope(envelopeSuccess);

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}