﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using Foundation.Core;
using Foundation.WebApi;
using Newtonsoft.Json;

namespace Foundation.WebApi
{
	public class ApiMessageProcessingHandler : MessageProcessingHandler
	{
		private const bool useRelativeUris = false;
		protected override HttpRequestMessage ProcessRequest(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			return request;
		}

		protected override HttpResponseMessage ProcessResponse(HttpResponseMessage response, CancellationToken cancellationToken)
		{
			var request = response.RequestMessage;
			var isList = true;

			if (response.Content == null || response.Content is StreamContent)
			{
				return response;
			}

			dynamic content = ((System.Net.Http.ObjectContent)(response.Content)).Value;

			if (content == null)
			{
				return new HttpResponseMessage(HttpStatusCode.NotFound);
			}

			if (!(content is IEnumerable) || (content is ExpandoObject))
			{
				content = new List<object> { ((System.Net.Http.ObjectContent)(response.Content)).Value };
				isList = false;
			}

			Type[] genericArgs = content.GetType().GetGenericArguments();
			dynamic contentResults = new List<object>();

			if (genericArgs != null && genericArgs.Any())
			{
				//var contentType = genericArgs.First();

				 var json1 = JsonConvert.SerializeObject(content, new JsonSerializerSettings()
				{
					NullValueHandling = NullValueHandling.Ignore,
					ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
					//ContractResolver = new CamelCasePropertyNamesContractResolver(),
					Formatting = Formatting.Indented,
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
				});

				var dynamicContent = JsonConvert.DeserializeObject<List<Dynamite>>(json1);

				foreach (var entity in dynamicContent)
				{
					if (entity != null)
					{
						contentResults.Add(ApiProjector.Convert(entity, response));
					}
				}

				if (! response.RequestMessage.GetUseApiEnvelope())
				{
					if (! isList)
					{
						contentResults = ((List<object>)contentResults).FirstOrDefault();
					}
					response.SetContent(contentResults as object);
				}
				else
				{
					var totalResults = request.GetTotalItemCount();
					int? currentCount = null;
					if (totalResults != null)
					{
						currentCount = contentResults.Count;
					}

					var apiResponse = new
					{
						Meta = new
						{
							Success = response.IsSuccessStatusCode,
							ApiVersion = 1,
							StatusCode = ((Int32) response.GetRequestedStatusCode()),
						},
						Items = contentResults,
						TotalResults = totalResults ?? 0,
						ItemsPerPage = currentCount ?? 0,
						Links = new
						{
							Self = useRelativeUris ? response.RequestMessage.RequestUri.ToRelativeUri() : response.RequestMessage.RequestUri,
							Root = useRelativeUris ? GlobalConfiguration.Configuration.VirtualPathRoot.ToAbsoluteUri() : String.Empty.ToAbsoluteUri(),
							Next = useRelativeUris ? request.GetNextPageRelativeUri() : request.GetNextPageAbsoluteUri(),
							Previous = useRelativeUris ? request.GetPreviousPageRelativeUri() : request.GetPreviousPageAbsoluteUri(),
						}
					};
					response.StatusCode = (response.StatusCode != HttpStatusCode.OK) ? response.StatusCode : response.GetRequestedStatusCode();
					response.SetContent(apiResponse);
				}

			}

			return response;
		}



	}
}