﻿using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.ServiceModel.Security.Tokens;
using Foundation.Core;

namespace Foundation.WebApi
{
    public class GetvJwtSecurityTokenHandler : JwtSecurityTokenHandler
    {
        /// <summary>
        /// Test the token for validity. Note, does NOT test to see if token 'jwt' is expired!
        /// This update fixes "Invalid Token" errors caused by renamed validation parameters.. ===JFE 2014-3-27
        /// </summary>
        /// <param name="jwt"></param>
        /// <returns></returns>
        public override ClaimsPrincipal ValidateToken(JwtSecurityToken jwt)
        {
            try
            {
                string jwtTrustedIssuer = ConfigurationManager.AppSettings.Get("JwtTrustedIssuer");
                string jwtSymmetricKey = ConfigurationManager.AppSettings.Get("JwtSymmetricKey");
                string jwtAudience = ConfigurationManager.AppSettings.Get("JwtAudience");
                BinarySecretSecurityToken signingToken = null;

                if (jwtSymmetricKey.IsBase64Encoded())
                {
                    signingToken = new BinarySecretSecurityToken(Convert.FromBase64String(jwtSymmetricKey));
                }

                var validationParameters = new TokenValidationParameters()
                {
                    AllowedAudience = jwtAudience,
                    SigningToken = signingToken,
                    ValidIssuer = jwtTrustedIssuer
                };

                NameClaimType = ClaimTypes.NameIdentifier;

                return base.ValidateToken(jwt, validationParameters);
            }
            catch (Exception)
            {
                return null;
            }

        }
    }
}