﻿using System.Linq;
using System.Security.Claims;
using System.Diagnostics;

namespace Identity.Client
{
	public class AuthorizationManager : ClaimsAuthorizationManager
	{
		/// <summary>
		/// When implemented in a derived class, checks authorization for the subject in the specified context to perform the specified action on the specified resource.
		/// </summary>
		/// <returns>
		/// true if the subject is authorized to perform the specified action on the specified resource; otherwise, false.
		/// </returns>
		/// <param name="context">The authorization context that contains the subject, resource, and action for which authorization is to be checked.</param>
		public override bool CheckAccess(AuthorizationContext context)
		{


			Trace.WriteLine("\n\nClaimsAuthorizationManager\n_______________________\n");

			Trace.WriteLine("\nAction:");
			Trace.WriteLine("  " + context.Action.First().Value);

			Trace.WriteLine("\nResources:");
			foreach (var resource in context.Resource)
			{
				Trace.WriteLine("  " + resource.Value);
			}

			Trace.WriteLine("\nClaims:");
			foreach (var claim in context.Principal.Claims)
			{
				Trace.WriteLine("  " + claim.Value);
			}

			var action = context.Action.First().Value;
			var identity = context.Principal.Identity;

			switch (action)
			{

				//case CustomClaimOperations.AllowAnonymous:
				//	if (!identity.IsAuthenticated)
				//	{
				//		return false;
				//	}

				//	break;

				case CustomRights.PossessProperty:
					if (context.Resource.Any(resource => ! context.Principal.HasClaim(c => c.Type == resource.Type)))
					{
						return false;
					}
					break;
			}

			context.Principal.HasClaim(c => c.Type == ClaimTypes.Anonymous);
			//return true;

			return identity.IsAuthenticated;

			return base.CheckAccess(context);
		}
	}
}