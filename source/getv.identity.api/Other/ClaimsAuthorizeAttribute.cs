﻿using System;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using Identity.Api;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Diagnostics;


namespace Foundation.WebApi
{
    public class ClaimsAuthorizeAttribute : Thinktecture.IdentityModel.Authorization.WebApi.ClaimsAuthorizeAttribute
    {

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            // inspect the request for token
            if (actionContext.Request.Headers.Authorization == null)
            {
                return false;
            }

            return CheckAccess(actionContext);

        }

        protected override bool CheckAccess(HttpActionContext actionContext)
        {
            //  Future Use TODO : Build Migration
            //BlacklistContext blacklistcontext = new BlacklistContext();

            try
            {
                var action = actionContext.ActionDescriptor.ActionName;
                var resource = actionContext.ControllerContext.ControllerDescriptor.ControllerName;

                // determine if user or client
                if (actionContext.Request.Headers.Authorization.ToString().Contains("Bearer"))
                {
                    var authorizationHeader = actionContext.Request.Headers.Authorization;

                    if (authorizationHeader != null && authorizationHeader.Scheme == "Bearer")
                    {

                        JwtSecurityToken token = new JwtSecurityToken(authorizationHeader.Parameter);

                        // TODO: Future Use = Blacklist
                        // Check the token/refreshToken against a blacklist/whitelist
                        //var badtoken =
                        //    blacklistcontext.Set<Blacklist>()
                        //        .Where(c => (c.BlacklistType == "Token".ToUpper() || c.BlacklistType =="RefreshToken") && c.BlacklistItem == authorizationHeader.Parameter);

                        //if (badtoken.Any())
                        //{
                        //    return false;
                        //}

                        if (token.ValidTo.ToUniversalTime() <= DateTime.Now.ToUniversalTime())
                        {
                            return false;
                        }

                        ClaimsPrincipal claimsprincipal = new GetvJwtSecurityTokenHandler().ValidateToken(token);
                        SetPrincipal(actionContext.Request, claimsprincipal);
                        return (claimsprincipal != null);

                    }

                }
            }
            catch (Exception ex)
            {
                var exEnumerator = ex;
                return false;
            }

            return false;

        }

        private void SetPrincipal(HttpRequestMessage request, ClaimsPrincipal principal)
        {
            System.Security.Claims.Claim testClaim = null;
            if (principal.Identity.IsAuthenticated)
            {
                var name = "unknown";

                if (!string.IsNullOrWhiteSpace(principal.Identity.Name))
                {
                    name = principal.Identity.Name;
                }
                else if (((testClaim = principal.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)) != null) && !string.IsNullOrWhiteSpace(testClaim.Value))
                //else if (  testClaim = principal.FindFirst(ClaimTypes.NameIdentifier) &&   !string.IsNullOrWhiteSpace(principal.FindFirst(ClaimTypes.NameIdentifier).))
                {
                    name = principal.Identity.Name;
                }
                else if (((testClaim = principal.FindFirst("sub")) != null) && !string.IsNullOrWhiteSpace(testClaim.Value))
                //else if (!string.IsNullOrWhiteSpace(principal.FindFirst("sub").Value))
                {
                    name = principal.Identity.Name;
                }
                else if (principal.Claims.First() != null)
                {
                    name = principal.Claims.First().Value;
                }

                Debug.WriteLine("Principal set for: " + name);
            }
            else
            {
                Debug.WriteLine("Setting anonymous principal.");
            }

            // set the ClaimsPrincipal on HttpContext.Current if the app is running in web hosted environment.
            if (HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }

            request.GetRequestContext().Principal = principal;

            //set the ClaimsPrincipal on the current thread.
            Thread.CurrentPrincipal = principal;


        }


        ///// <summary>
        ///// Processes requests that fail authorization.
        ///// </summary>
        ///// <param name="actionContext">The context.</param>
        //protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        //{
        //    if (_action == null && _resources == null)
        //    {
        //        var error = new HttpError { Message = "unauthorized_access", MessageDetail = "access is denied." };
        //        var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
        //        actionContext.Response = response;
        //    }
        //    else
        //    {
        //        var error = new HttpError { Message = "missing_claim", MessageDetail = "A required claim for this operation is missing." };
        //        var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
        //        actionContext.Response = response;
        //    }

        //}



        // ------   CHECK ACCESS ----- //



        ///// <summary>
        ///// Handle a single resource request from a User
        ///// </summary>
        ///// <param name="action"></param>
        ///// <param name="resource"></param>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public bool VerifyClaim(HttpActionContext receivedActionContext, System.IdentityModel.Claims.Claim claim)
        //{
        //    // BlacklistContext blacklistcontext = new BlacklistContext();

        //    //// determine if user or client
        //    //if (receivedActionContext.Request.Headers.Authorization.ToString().Contains("Bearer"))
        //    //{

        //    //    var authorizationHeader = receivedActionContext.Request.Headers.Authorization;

        //    //    var rawToken = authorizationHeader.Parameter;

        //    //    if (authorizationHeader != null && authorizationHeader.Scheme == "Bearer")
        //    //    {

        //    //        JwtSecurityToken token = new JwtSecurityToken(authorizationHeader.Parameter);

        //    //        // Check the token against a blacklist/whitelist
        //    //        var badtoken =
        //    //            blacklistcontext.Set<Blacklist>()
        //    //                .Where(
        //    //                    c =>
        //    //                        (c.BlacklistType == "Token".ToUpper() &
        //    //                            c.BlacklistItem == authorizationHeader.Parameter));

        //    //        // Check Refresh Token

        //    //        if (badtoken.Any())
        //    //        {
        //    //            return false;
        //    //        }

        //    //        if (token.ValidTo.ToUniversalTime() <= DateTime.Now.ToUniversalTime())
        //    //        {
        //    //            // async update of blacklist/whitelist
        //    //            // Wait, why isn't ValidateToken including date/time checking? is it UTC time?
        //    //            return false;
        //    //        }

        //    //        //  var claimsPrincipal = token == null ? Principal.Anonymous : 
        //    //        ClaimsPrincipal claimsprincipal = new GetvJwtSecurityTokenHandler().ValidateToken(token);

        //    //        //SetPrincipal(request, claimsPrincipal);
        //    //        if (claimsprincipal != null)
        //    //        {   

        //    //            foreach (System.Security.Claims.Claim checkclaim in token.Claims)
        //    //            {
        //    //                string claimType = claim.ClaimType;

        //    //                string parseClaim = String.Format("{0}.{1}",claim.Resource, claim.Right);

        //    //                if  ( (checkclaim.Type == claim.ClaimType) && (checkclaim.Value == parseClaim))
        //    //                {
        //    //                    return true;
        //    //                }
        //    //            }

        //    //        }
        //    //        else
        //    //        {
        //    //            return false;
        //    //        }

        //    //    }
        //    //    return false;

        //    //}
        //    //else
        //    //{
        //    //    return false;
        //    //}

        //    return true;

        //}

        /// <summary>
        /// Handle a ,ultiple resource request fom a User
        /// </summary>
        /// <param name="action"></param>
        /// <param name="resource"></param>
        /// <param name="user"></param>
        ///// <returns></returns>
        //public bool CheckUserAccess(HttpActionContext actionContext, string action, string[] resource, User user)
        //{
        //    return true;
        //}
    }

}