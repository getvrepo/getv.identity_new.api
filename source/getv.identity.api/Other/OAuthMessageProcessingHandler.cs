﻿using System;
using System.Diagnostics;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Thinktecture.IdentityModel;
using Configuration = Formo.Configuration;

namespace Identity.Client
{
	public class OAuthMessageProcessingHandler : DelegatingHandler
	{

		/// <summary>
		/// Sends an HTTP request to the inner handler to send to the server as an asynchronous operation.
		/// </summary>
		/// <returns>
		/// Returns <see cref="T:System.Threading.Tasks.Task`1"/>. The task object representing the asynchronous operation.
		/// </returns>
		/// <param name="request">The HTTP request message to send to the server.</param><param name="cancellationToken">A cancellation token to cancel operation.</param><exception cref="T:System.ArgumentNullException">The <paramref name="request"/> was null.</exception>
		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{

			base.SendAsync(request, cancellationToken);

			dynamic config = new Configuration();
            // Default token required to true
            var requireAccessToken = config.AccessTokenRequired<bool>(true);
			var tokenHandler = new GetvJwtSecurityTokenHandler();

			try
			{
				var token = ExtractJwtTokenFromHeader(request);
				if (requireAccessToken && token == null)
				{
					var error = new HttpError { Message = "missing_token", MessageDetail = "The access token is required" };
					var response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
					return Task.FromResult(response);
				}

                // Web.config contains default keys & seed for for creating security (i.e. new BinarySecretSecurityToken)
                var claimsPrincipal = token == null ? Principal.Anonymous : tokenHandler.ValidateToken(token);
				SetPrincipal(request, claimsPrincipal);

			}
			catch (Exception ex)
			{
				HttpResponseMessage response;
				if (ex.Message.Contains("Jwt10113"))
				{
					var error = new HttpError { Message = "invalid_token", MessageDetail = "The access token is invalid" };
					response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
				}

				// Jwt10315: Signature validation failed.
				else if (ex.Message.Contains("Jwt10315"))
				{
					var error = new HttpError { Message = "invalid_token", MessageDetail = "The access token is invalid" };
					response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
				}

				else if (ex.Message.Contains("Jwt10305"))
				{
					var error = new HttpError { Message = "expired_token", MessageDetail = "The access token expired" };
					response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
				}

				else if (ex is SecurityTokenValidationException)
				{
					var error = new HttpError { Message = "unauthorized_access", MessageDetail = "access is denied." };
					response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
				}
				else
				{
					var error = new HttpError { Message = "system_error", MessageDetail = "A system error has occurred.  Please try your request again later." };
					response = request.CreateErrorResponse(HttpStatusCode.InternalServerError, error);
				}
				return Task.FromResult(response);
			}

			try
			{
				var result = base.SendAsync(request, cancellationToken);
				return result;
			}
			catch (Exception ex)
			{
				var error = new HttpError { Message = "invalid_token", MessageDetail = "This is messed up..." };
				var response = request.CreateErrorResponse(HttpStatusCode.Unauthorized, error);
				return Task.FromResult(response);
			}

		}


		private void SetPrincipal(HttpRequestMessage request, ClaimsPrincipal principal)
		{
			Claim testClaim = null;
			if (principal.Identity.IsAuthenticated)
			{
				var name = "unknown";

				if (!string.IsNullOrWhiteSpace(principal.Identity.Name))
				{
					name = principal.Identity.Name;
				}
				else if (((testClaim = principal.FindFirst(ClaimTypes.NameIdentifier)) != null) && !string.IsNullOrWhiteSpace(testClaim.Value))
				//else if (  testClaim = principal.FindFirst(ClaimTypes.NameIdentifier) &&   !string.IsNullOrWhiteSpace(principal.FindFirst(ClaimTypes.NameIdentifier).))
				{
					name = principal.Identity.Name;
				}
					else if (((testClaim = principal.FindFirst("sub")) != null) && !string.IsNullOrWhiteSpace(testClaim.Value))
				//else if (!string.IsNullOrWhiteSpace(principal.FindFirst("sub").Value))
				{
					name = principal.Identity.Name;
				}
				else if (principal.Claims.First() != null)
				{
					name = principal.Claims.First().Value;
				}

				Debug.WriteLine("Principal set for: " + name);
			}
			else
			{
				Debug.WriteLine("Setting anonymous principal.");
			}

			// set the ClaimsPrincipal on HttpContext.Current if the app is running in web hosted environment.
			if (HttpContext.Current != null)
			{
				HttpContext.Current.User = principal;
			}

			request.GetRequestContext().Principal = principal;

			//set the ClaimsPrincipal on the current thread.
			Thread.CurrentPrincipal = principal;

		}

		static JwtSecurityToken ExtractJwtTokenFromHeader(HttpRequestMessage request)
		{
			request.GetClaimsPrincipal();

			var authorizationHeader = request.Headers.Authorization;

            if (authorizationHeader != null && authorizationHeader.Scheme == "Bearer")
            {
                return new JwtSecurityToken(authorizationHeader.Parameter);
            }

            return null;
		}
	}
}