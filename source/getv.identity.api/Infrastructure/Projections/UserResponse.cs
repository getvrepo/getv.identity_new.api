﻿using System.Net.Http;
using Foundation.WebApi;

namespace Identity.Api
{
	public class UserResponse : ApiProjector
	{

		protected override object ConvertItem(dynamic entity, HttpResponseMessage response)
		{
			var result = new
			{               
				Username = entity.UserName,
				FirstName = entity.FirstName,
				LastName = entity.LastName ,
				FullName = entity.FullName,
				EmailAddress= entity.EmailAddress,
				IsEmailVerified  = entity.IsEmailVerified,

				EmailVerificationToken=entity.EmailVerificationToken,
				PasswordHash = entity.PasswordHash,            
				PasswordVerificationToken   = entity.PasswordVerificationToken,
				PasswordVerificationTokenExpirationDate   = entity.PasswordVerificationTokenExpirationDate,
				LastPasswordChangedDate   = entity.LastPasswordChangedDate,
				FailedPasswordAttemptCount  = entity.FailedPasswordAttemptCount,
				MustChangePasswordOnLogin  = entity.MustChangePasswordOnLogin,
				FailedPasswordAttemptWindowStart  = entity.FailedPasswordAttemptWindowStart,
  
				Claims = entity.Claims.ToArray(),
				Roles   = entity.Roles.ToArray()
			};

			return result;

		}

	}

}