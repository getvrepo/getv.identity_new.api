﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Foundation.Core;
using Microsoft.Owin.Security.OAuth;

namespace Identity.Api
{
	public static class IdentityExtensions
	{
		public static void AddScope(this Application application, string scopeName, string description = null)
		{
			application.Scopes = application.Scopes ?? new List<Scope>();

			var scope = new Scope();
			scope.Id = scopeName;
			scope.Name = scopeName.Replace("-", " ");
			scope.Description = scope.Description ?? scope.Name;
			scope.Initialize();

			application.Scopes.Add(scope);

		}

		public static void AddScope(this Client client, string scopeName)
		{
			client.Scopes = client.Scopes ?? new List<Scope>();
			var scope = client.Application.Scopes.FirstOrDefault(s => s.Name == scopeName);
			client.Scopes.Add(scope);
		}


		public static void SetPassword(this Client client, string password, string salt = null, string hashname = "pbkdf2hmacsha256", int iterations = 5000)
		{
			var passwordHash = PasswordHasher<Pbkdf2>.HashPassword(password);
			client.ClientSecret = passwordHash;
		}

		public static void SetPassword(this User user, string password, string salt = null, string hashname = "pbkdf2hmacsha256", int iterations = 5000)
		{
			var passwordHash = PasswordHasher<Pbkdf2>.HashPassword(password);
			user.PasswordHash = passwordHash;
		}

		public static void SetSigningKey(this Application application, string signingKey)
		{
			application.SigningKey = Convert.FromBase64String(signingKey);
		}

		public static string GetEnumDescription(this Enum value)
		{
			var fi = value.GetType().GetField(value.ToString());

			var attributes =
				(DescriptionAttribute[])fi.GetCustomAttributes(
				typeof(DescriptionAttribute),
				false);

			return attributes.Length > 0 ? attributes[0].Description : value.ToString();
		}


		public static bool TryGetBasicCredentials(this OAuthGrantClientCredentialsContext context,   out string clientId, out string clientSecret)
		{
			// Client Authentication http://tools.ietf.org/html/rfc6749#section-2.3
			// Client Authentication Password http://tools.ietf.org/html/rfc6749#section-2.3.1
			string authorization = context.OwinContext.Request.Headers.Get("Authorization");
			if (!string.IsNullOrWhiteSpace(authorization) && authorization.StartsWith("Basic ", StringComparison.OrdinalIgnoreCase))
			{
				byte[] data = Convert.FromBase64String(authorization.Substring("Basic ".Length).Trim());
				string text = Encoding.UTF8.GetString(data);
				int delimiterIndex = text.IndexOf(':');
				if (delimiterIndex >= 0)
				{
					clientId = text.Substring(0, delimiterIndex);
					clientSecret = text.Substring(delimiterIndex + 1);
					//ClientId = clientId;
					return true;
				}
			}

			clientId = null;
			clientSecret = null;
			return false;
		}


	}
}