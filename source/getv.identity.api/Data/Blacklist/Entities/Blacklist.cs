﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity.Api
{
    /// <summary>
    /// Build Lists of Blacklisted items within a date range by querying this data source
    ///     Maintenance:    
    ///             Refresh Tokens should only stay on the blacklist for Lifetime + x and get removed
    ///     Common BlacklistTypes:
    ///             EmailAddress
    ///             Token (mostly Identity API - Immediate lockout feature)
    ///             RefreshToken 
    ///             Keywords (i.e. Prayer Request Filter words)
    ///      
    ///     Keep in memory and refresh from DB ocassionally?       
    /// 
    ///    TODO  JFE - build an Enum set for Blacklist Types but leave 'BlacklistItem' as string for agility
    ///     ===JFE 2014-03-26
    /// </summary>
	[Table("Blacklist")]
    public class Blacklist : BaseEntity
	{
        [MaxLength(100)]
		[Required]
		public virtual string BlacklistType { get; set; }

        [Required]
        [MaxLength(2000)]
		public virtual string BlacklistItem { get; set; }

	    // The DateTime when the item is to be automatically removed
        // Especially important for blacklisted RefreshTokens
        public virtual DateTime? ParoleDateTime { get; set; }
		[MaxLength(100)]
		public virtual string Reason { get; set; }

        [MaxLength(2000)]
	    public virtual string Notes { get; set; }

        //// Possible future use - limit to specific applications, for now mostly null   ===JFE 2014-03-26
        //public virtual List<Application> Application { get; set; }

        //// Possible future use - limit to specific clients, for now mostly null   ===JFE 2014-03-26
        //public virtual List<Client> Client { get; set; }

	}

}