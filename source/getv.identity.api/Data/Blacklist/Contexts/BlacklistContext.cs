﻿using System.Data.Entity;

namespace Identity.Api
{
	public class BlacklistContext : DbContext
	{ 
		static BlacklistContext()
		{
			Database.SetInitializer<BlacklistContext>(new DropCreateDatabaseIfModelChanges<BlacklistContext>());
		}

        public virtual DbSet<Blacklist> Blacklist { get; set; }

		public BlacklistContext()
		{
			Configuration.LazyLoadingEnabled = false;
			Configuration.ProxyCreationEnabled = false;
		}
	}
}