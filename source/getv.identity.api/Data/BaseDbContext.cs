﻿using System.Data.Entity;

namespace Foundation.EntityFramework
{
	public class BaseDbContext<TContext> : DbContext where TContext : DbContext, new()
	{
		public static void Initialize()
		{
			using (var context = new TContext())
			{
			//	context.Database.Initialize(false);
			}
		}
	}
}