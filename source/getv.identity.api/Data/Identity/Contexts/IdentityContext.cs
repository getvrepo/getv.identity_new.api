using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Foundation.EntityFramework;
using Identity.Api.Data.Identity.Contexts;

namespace Identity.Api
{
	public class IdentityContext : BaseDbContext<IdentityContext>
	{

		static IdentityContext()
		{
			Database.SetInitializer(new IdentityContextInitializer());
		}

		public IdentityContext()
		{
			Configuration.LazyLoadingEnabled = false;
			Configuration.ProxyCreationEnabled = false;
  		    var cls = new DbMigrationsConfiguration<IdentityContext>();
		    cls.AutomaticMigrationsEnabled = true;
			// Database.SetInitializer<IdentityContext>(new IdentityContextInitializer());
		}

		public virtual DbSet<Application> Applications { get; set; }

		public virtual DbSet<User> Users { get; set; }

        public DbSet<Scope> Scopes { get; set; }

        public DbSet<ClaimEntity> ClaimEntities { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Client> Clients { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
            modelBuilder.HasDefaultSchema("api");

            modelBuilder.Properties<Guid>()
            .Where(p => p.Name == "PrimaryKey")
            .Configure(p => p.IsKey());


            modelBuilder.Entity<User>()
            .HasMany(c => c.Claims)
            .WithMany(u => u.Users)
            .Map(x =>
            {
	            x.MapLeftKey("User_PrimaryKey");
	            x.MapRightKey("Claim_PrimaryKey");
	            x.ToTable("User_Claims");
            });

            modelBuilder.Entity<Client>()
            .HasMany(c => c.Scopes)
            .WithMany(u => u.AllowedClients)
            .Map(x =>
            {
	            x.MapLeftKey("Client_PrimaryKey");
	            x.MapRightKey("Scope_PrimaryKey");
	            x.ToTable("Client_Scopes");
            });

            modelBuilder.Entity<Role>()
            .HasMany(c => c.Users)
            .WithMany(u => u.Roles)
            .Map(x =>
            {
                x.MapLeftKey("User_PrimaryKey");
                x.MapRightKey("Role_PrimaryKey");
                x.ToTable("User_Roles");
            });

            base.OnModelCreating(modelBuilder);
		}
	}
}