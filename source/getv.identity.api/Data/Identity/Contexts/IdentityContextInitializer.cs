﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Identity.Api.Migrations;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace Identity.Api.Data.Identity.Contexts
{
	public class IdentityContextInitializer : MigrateDatabaseToLatestVersion<IdentityContext, MigrationsConfiguration>
	{
		private const string GetvUrl = "https://test.getv.org/Admin/Json/";

		//private const string GetvUrl= "http://localhost:2106/Admin/Json/";

        //protected void Seed(IdentityContext context)
        //{
        //    base.Seed(context);

        //    // ImportUsers(context);

        //    SeedData(context);
        //}

		private void SeedData(IdentityContext context)
		{
			Application application;
			ClaimEntity claim;
			User user;
			Client client;

			application = context.Set<Application>().Create();
			application.PrimaryKey = new Guid("640cd0e2-6fe5-4646-873f-fac0cf639342");
			application.Name = "Superhero Application";
			application.Id = "superhero-app";
			application.Description = "Application for Superheroes";
			application.Audience = "http://www.superherostudios.com";
			application.Issuer = "http://identity.superherostudios.com";
			application.SetSigningKey("Dc9Mpi3jbooUpBQpB/4R7XtUsa3D/ALSjTVvK8IUZbg=");
			application.AccessTokenLifetime = 10;  // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			application.RefreshTokenLifetime = 10080;  // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			application.Users = new List<User>();
			application.Clients = new List<Client>();
			application.AddScope("users.update");
			application.AddScope("users.read");
			application.Initialize();


			client = new Client();
			client.Application = application;
			client.ClientId = "superhero-mobile-id";
			client.Name = "Superhero Mobile Application";
			client.Initialize();
			client.SetPassword("superhero-mobile-secret");
			client.AllowRefreshToken = true;
			client.AccessTokenLifetime = 1440; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.RefreshTokenLifetime = 10080 * 2; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.AddScope("users.update");
			client.AddScope("users.read");
			application.Clients.Add(client);

			client = new Client();
			client.Application = application;
			client.ClientId = "superhero-web-id";
			client.Name = "Superhero Web Application";
			client.Initialize();
			client.SetPassword("superhero-web-password");
			client.AllowRefreshToken = true;
			client.AccessTokenLifetime = 1440; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.RefreshTokenLifetime = 10080 * 2; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.AddScope("users.update");
			client.AddScope("users.read");
			application.Clients.Add(client);


            user = new User();
            user.Id = "00000000";
            user.FirstName = "Tseug";
            user.LastName = "Fdsa";
            user.Username = "guest";
            user.EmailAddress = "Tseug.Fdsa@keyboardMagic.com";
            user.SetPassword("asdfasdf");
            user.Initialize();
            application.Users.Add(user);

            user = new User();
            user.Id = "01010101";
            user.FirstName = "Tony";
            user.LastName = "Stark";
            user.Username = "tonystark";
            user.EmailAddress = "tony.stark@starkindustries.com";
            user.SetPassword("ironman");
            user.Initialize();
            application.Users.Add(user);

            user = new User();
            user.Id = "111111";
            user.FirstName = "Peter";
            user.LastName = "Parker";
            user.Username = "peterparker";
            user.EmailAddress = "peter.parker@superherostudios.com";
            user.SetPassword("spiderman");
            user.Initialize();
            application.Users.Add(user);

            user = new User();
            user.Id = "222222";
            user.FirstName = "Clark";
            user.LastName = "Kent";
            user.Username = "clarkkent";
            user.EmailAddress = "clark.kent@superherostudios.com";
            user.SetPassword("superman");
            user.Initialize();
            application.Users.Add(user);

			user = new User();
			user.Id = "333333";
			user.FirstName = "Bruce";
			user.LastName = "Wayne";
			user.Username = "brucewayne";
			user.EmailAddress = "bruce.wayne@superherostudios.com";
			user.SetPassword("batman");
			user.Initialize();
			application.Users.Add(user);


			context.Set<Application>().AddOrUpdate(application);
			context.SaveChanges();

			application = context.Set<Application>().Create();
			application.PrimaryKey = new Guid("8efaf1c4-c860-4a91-95d9-a229c15a3146");
			application.Name = "JHM Unified Website";
			application.Id = "jhm-unified-website";
			application.Description = "Unified Website for John Hagee Ministries";
			application.Audience = "http://www.jhm.org";
			application.Issuer = "http://identity.jhm.org";
			application.AccessTokenLifetime = 10;
			//signingKey = Convert.ToBase64String(Encoding.UTF8.GetBytes("27SJTUxeB87rzoW5ruaXwQ1A2AfY8WYL79u0OrxP41ZzMEyNlY"));
			application.SetSigningKey("MjdTSlRVeGVCODdyem9XNXJ1YVh3UTFBMkFmWThXWUw3OXUwT3J4UDQxWnpNRXlObFk=");
			application.Users = new List<User>();
			application.Clients = new List<Client>();
			application.AddScope("videos.read");
			application.AddScope("videos.update");
			application.AddScope("videos.create");
			application.AddScope("videos.delete");
			application.AddScope("videos.admin");
			application.AddScope("products.read");
			application.AddScope("products.update");
			application.AddScope("products.create");
			application.AddScope("products.delete");
			application.AddScope("products.admin");
			application.AddScope("users.read");
			application.AddScope("users.update");
			application.AddScope("users.create");
			application.AddScope("users.delete");
			application.AddScope("users.admin");
			application.AddScope("orders.read");
			application.AddScope("orders.update");
			application.AddScope("orders.create");
			application.AddScope("orders.delete");
			application.AddScope("orders.admin");
			application.AddScope("orders.read");
			application.AddScope("events.read");
			application.AddScope("events.update");
			application.AddScope("events.create");
			application.AddScope("events.delete");
			application.AddScope("events.admin");
			application.AddScope("publications.read");
			application.AddScope("publications.update");
			application.AddScope("publications.create");
			application.AddScope("publications.delete");
			application.AddScope("publications.admin");
			application.AddScope("messages.read");
			application.AddScope("messages.update");
			application.AddScope("messages.create");
			application.AddScope("messages.delete");
			application.AddScope("messages.admin");
			application.AddScope("posts.read");
			application.AddScope("posts.update");
			application.AddScope("posts.create");
			application.AddScope("posts.delete");
			application.AddScope("posts.admin");
            application.AddScope("dashboard.read");
            application.AddScope("dashboard.update");
            application.AddScope("dashboard.create");
            application.AddScope("dashboard.delete");
            application.AddScope("dashboard.admin");
			application.Initialize();

			claim = context.Set<ClaimEntity>().Create();
			claim.PrimaryKey = new Guid();
			claim.Resource = "products";


			client = new Client();
			client.Application = application;
			client.ClientId = "jhm-mobile";
			client.Name = "JHM Mobile Application";
			client.Initialize();
			client.SetPassword("Juxl1XpywdsJsBvfSzWBeqsZBrPP7coKC7GYgCr4zR9kOEUD1T");
			client.AllowRefreshToken = true;
			client.AccessTokenLifetime = 1440; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.RefreshTokenLifetime = 10080 * 2; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.AddScope("videos.read");
			client.AddScope("users.read");
			client.AddScope("events.read");
			client.AddScope("posts.read");
			client.AddScope("messages.read");
			client.AddScope("messages.create");
			client.AddScope("publications.read");
			application.Clients.Add(client);

			client = new Client();
			client.Application = application;
			client.ClientId = "jhm-web";
			client.Name = "JHM Web Application";
			client.Initialize();
			client.SetPassword("CQ9BHa65o3wH4ikQl5NIlIzHI47RrQEgeV9yLuXYghHEictGEu");
			client.AllowRefreshToken = true;
			client.AccessTokenLifetime = 10080; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.RefreshTokenLifetime = 10080 * 2; // 1 Day = 1440, 1 Week = 10080, 1 Year = 524160
			client.AddScope("videos.read");
			client.AddScope("videos.update");
			client.AddScope("videos.create");
			client.AddScope("videos.delete");
			client.AddScope("videos.admin");
			client.AddScope("products.read");
			client.AddScope("products.update");
			client.AddScope("products.create");
			client.AddScope("products.delete");
			client.AddScope("products.admin");
			client.AddScope("users.read");
			client.AddScope("users.update");
			client.AddScope("users.create");
			client.AddScope("users.delete");
			client.AddScope("users.admin");
			client.AddScope("orders.read");
			client.AddScope("orders.update");
			client.AddScope("orders.create");
			client.AddScope("orders.delete");
			client.AddScope("orders.admin");
			client.AddScope("orders.read");
			client.AddScope("events.read");
			client.AddScope("events.update");
			client.AddScope("events.create");
			client.AddScope("events.delete");
			client.AddScope("events.admin");
			client.AddScope("publications.read");
			client.AddScope("publications.update");
			client.AddScope("publications.create");
			client.AddScope("publications.delete");
			client.AddScope("publications.admin");
			client.AddScope("messages.read");
			client.AddScope("messages.update");
			client.AddScope("messages.create");
			client.AddScope("messages.delete");
			client.AddScope("messages.admin");
			client.AddScope("posts.read");
			client.AddScope("posts.update");
			client.AddScope("posts.create");
			client.AddScope("posts.delete");
			client.AddScope("posts.admin");
            client.AddScope("dashboard.read");
            client.AddScope("dashboard.update");
            client.AddScope("dashboard.create");
            client.AddScope("dashboard.delete");
            client.AddScope("dashboard.admin");
			application.Clients.Add(client);

			context.Set<Application>().AddOrUpdate(application);
			context.SaveChanges();

		}

        //private void ImportUsers(IdentityContext context)
        //{
        //    var securityUsers = securityContext.Set<GetvUserEntity>()
        //        .Where(e => e.IsActive);


        //    foreach (var securityUser in securityUsers)
        //    {
        //        var user = context.Set<User>().Create();
        //        user.Initialize();
        //        user.Username = securityUser.Username;
        //        user.FirstName = securityUser.FirstName;
        //        user.LastName = securityUser.LastName;
        //        user.EmailAddress = securityUser.EmailAddress;
        //        user.PasswordHash = securityUser.PasswordHash;
        //        user.IsEmailVerified = securityUser.IsEmailVerified;
        //        //user.Domain = "getv";
				
        //        context.Set<User>().Add(user);
        //        context.SaveChanges();
        //    }

		
        //}
	}
}