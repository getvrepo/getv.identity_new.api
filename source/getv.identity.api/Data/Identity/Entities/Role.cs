﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity.Api
{
    [Table("Roles")]
	public class Role : BaseEntity 
	{
		[Required]
		[StringLength(100)]
		public string Name { get; set; }

		[Required]
		[StringLength(500)]
		public string Description { get; set; }

		public ICollection<Client> AllowedClients { get; set; }

		public Application Application { get; set; }

        public ICollection<ClaimEntity> Claims { get; set; }

        public ICollection<User> Users { get; set; }
	
	}
}