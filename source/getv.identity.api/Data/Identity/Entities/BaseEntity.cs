﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;
using Foundation.Core;

namespace Identity.Api
{
	public abstract class BaseEntity
	{
		[Key]
		[Required]
		//[IgnoreDataMember]
		public Guid PrimaryKey { get; set; }

		[Required]
		public string Id { get; set; }

		private string _slug;
		public string Slug
		{
			get
			{
				return _slug ?? Id;
			}
			set
			{
				_slug = value;
			}
		}

		[DefaultValue(1000)]
		public int Weight { get; set; }

		[NotMapped]
		public string Kind
		{
			get
			{
				return this.GetType().Name.ToCamelCase();
			}

		}
		//public string Etag { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Date Created")]
		public DateTime? DateCreated { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Date Modified")]
		public DateTime? DateModified { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Created By")]
		//[MaxLength(50)]
		public string CreatedBy { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Modified By")]
		//[MaxLength(50)]
		public string ModifiedBy { get; set; }

		[Timestamp]
		public virtual Byte[] TimeStamp { get; set; }

		public bool Initialize()
		{
			this.PrimaryKey = (this.PrimaryKey != Guid.Empty) ? this.PrimaryKey : SequentialGuid.NewGuid();
			this.Id = this.Id ?? CalculateMD5Hash(this.PrimaryKey.ToString("N"));
			this.CreatedBy = this.CreatedBy ?? "system";
			this.ModifiedBy = this.ModifiedBy ?? "system";
			this.DateCreated = this.DateCreated ?? DateTime.Now;
			this.DateModified = this.DateModified ?? DateTime.Now;
			this.Slug = this.Slug ?? this.Id;
			this.Weight = 1000;

			return true;
		}

		public string CalculateMD5Hash(string input)
		{
			// step 1, calculate MD5 hash from input
			MD5 md5 = System.Security.Cryptography.MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				sb.Append(hash[i].ToString("x2"));
			}
			return sb.ToString();
		}

		[DefaultValue(false)]
		public bool IsRemoved { get; set; }

		[DefaultValue(true)]
		/// <summary>
		/// Indicates whether the client is enabled or not.
		/// </summary>
		public bool Enabled { get; set; }

	}
}