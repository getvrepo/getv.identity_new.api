﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Identity.Api
{
	public class Client : BaseEntity
	{
		[Required]
		[StringLength(250)]
		[Display(Name = "Client ID", Description = "Unique identifier for the client.")]
		public string ClientId { get; set; }

		[Required]
		[Display(Name = "Name", Description = "Display name for the client.")]
		public string Name { get; set; }


		[Display(Name = "Client Secret", Description = "Password for the client.")]
		public string ClientSecret { get; set; }

		[Display(Name = "Access Token Lifetime", Description = "Lifetime (in minutes) of access token issued to client.")]
		[Range(0, Int32.MaxValue)]
		public int AccessTokenLifetime { get; set; }

		/// <summary>
		/// Indicates whether the refresh token should be issued for this client or not.
		/// </summary>
		public bool AllowRefreshToken { get; set; }

		[Display(Name = "Refresh Token Lifetime", Description = "Lifetime (in minutes) of refresh token issued to client. Only allowed for code flow clients.")]
		[Range(0, Int32.MaxValue)]
		public int RefreshTokenLifetime { get; set; }

		public Application Application { get; set; }

		public bool HasClientSecret { get; set; }

		public List<Scope> Scopes { get; set; }
	}


	
}