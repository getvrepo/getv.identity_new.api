﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity.Api
{
	[Table("Claims")]
	public class ClaimEntity : BaseEntity
	{
		[Required]
		public string ClaimType { get; set; }
		[Required]
		public string Resource { get; set; }
		[Required]
		public string Right { get; set; }

		public virtual List<User> Users { get; set; } 
	}
}