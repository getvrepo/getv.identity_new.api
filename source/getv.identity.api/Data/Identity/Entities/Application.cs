﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Identity.Api
{
	public class Application : BaseEntity, IValidatableObject
	{
		[Required]
		public string Name { get; set; }

		[StringLength(1000)]
		public string Description { get; set; }

		public string LogoUrl { get; set; }

		[Required]
		public string Audience { get; set; }

		[Required]
		public string Issuer { get; set; }

		[Range(1, Int32.MaxValue)]
		public int AccessTokenLifetime { get; set; }

		[Range(0, Int32.MaxValue)]
		public int RefreshTokenLifetime { get; set; }

		public bool AllowRefreshToken { get; set; }

		public ICollection<User> Users { get; set; }
		public ICollection<Client> Clients { get; set; }

		public Byte[] SigningKey { get; set; }

		public List<Scope> Scopes { get; set; }

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			var errors = new List<ValidationResult>();

			if (  String.IsNullOrWhiteSpace(this.Audience) )
			{
				errors.Add(new ValidationResult(Resources.Application.AudienceRequiredError, new string[] { "Audience" }));
			}


			return errors;
		}
	}
}