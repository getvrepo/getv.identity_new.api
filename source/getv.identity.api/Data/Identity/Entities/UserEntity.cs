﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity.Api
{
	[Table("Users")]
	public class User : BaseEntity
	{
		[Required]
		[MaxLength(100)]
		public virtual string Username { get; set; }

		//[Required]
		[MaxLength(100)]
		public virtual string FirstName { get; set; }


		//[Required]
		[MaxLength(100)]
		public virtual string LastName { get; set; }

		[NotMapped]
		public virtual string FullName
		{
			get
			{
				return (FirstName ?? String.Empty) + " " + (LastName ?? String.Empty);
			}
		}

		[Required]
		[Display(Name = "Email Address")]
		[MaxLength(256)]
		public virtual string EmailAddress { get; set; }


		[ScaffoldColumn(false)]
		[MaxLength(100)]
		public virtual string EmailVerificationToken { get; set; }

		[ScaffoldColumn(false)]
		[MaxLength(100)]
		public virtual string PasswordVerificationToken { get; set; }

		[ScaffoldColumn(false)]
		public virtual DateTime? PasswordVerificationTokenExpirationDate { get; set; }

		public virtual DateTime? LastPasswordChangedDate { get; set; }

		public virtual int FailedPasswordAttemptCount { get; set; }

		public virtual bool MustChangePasswordOnLogin { get; set; }

		public virtual DateTime? FailedPasswordAttemptWindowStart { get; set; }

		public virtual List<ClaimEntity> Claims { get; set; }

		[MaxLength(100)]
		public string PasswordHash { get; set; }

		public bool IsEmailVerified { get; set; }

        public virtual List<Role> Roles { get; set; } 
	}

}