﻿using HttpFormatting = System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.OData.Builder;
using Foundation.WebApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Identity.Api
{
	public static class WebApiConfig
	{
		public static HttpConfiguration Register()
		{
			var config = new HttpConfiguration();


            ////var cors = new EnableCorsAttribute("http://localhost:2038,http://localhost:1094", "*", "*");   // Example how to setup for multiple domains
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);



			config.MapHttpAttributeRoutes();

			config.EnableQuerySupport(new CustomQueryableAttribute());
            config.Filters.Add(new WebApiExceptionFilterAttribute());
			config.Filters.Add(new ClaimsAuthorizeAttribute());
            //config.MessageHandlers.Add(new OAuthMessageProcessingHandler());  / Overridden with unqiue ClaimsAuthorize
            config.MessageHandlers.Add(new ApiMessageProcessingHandler());

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "{controller}/{id}",
				defaults: new { controller = "StartUp", id = RouteParameter.Optional }
			);

			// Build the Route to Scopes Controller
			ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
			builder.EntitySet<Scope>("Scopes");
			builder.EntitySet<Client>("Client"); 
			builder.EntitySet<Application>("Application"); 
			builder.EntitySet<User>("Users");
			builder.EntitySet<ClaimEntity>("Claims");
		    builder.Action("GetStartUp");
			config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());

			var jsonFormatter = new HttpFormatting.JsonMediaTypeFormatter();
			jsonFormatter.SerializerSettings = new JsonSerializerSettings()
			{
				NullValueHandling = NullValueHandling.Ignore,
				ContractResolver = new CamelCasePropertyNamesContractResolver(),
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
				Formatting = Formatting.Indented,
				DateTimeZoneHandling = DateTimeZoneHandling.Utc,
			};

			config.Services.Replace(typeof(HttpFormatting.IContentNegotiator), new JsonContentNegotiator(jsonFormatter));

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();

			return config;
		}
	}
}
