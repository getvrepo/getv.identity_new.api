﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;

namespace Identity.Api.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using Identity.Api;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Client>("Client");
    builder.EntitySet<Application>("Application"); 
    builder.EntitySet<Scope>("Scope"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ClientController : ODataController
    {
        private IdentityContext db = new IdentityContext();

        // GET odata/Client
        [Route("Clients")]
        [Queryable]
        public IQueryable<Client> GetAllClients()
        {
            return db.Clients;
        }

        [Route("Client/{id}")]
        [Queryable]
        public Client GetClients(String id)
        {
            return db.Clients.Where(a=>a.Id == id).Include(s=>s.Scopes).FirstOrDefault();
        }

        // GET odata/Client(5)
        [Route("Client")]
        [Queryable]
        public SingleResult<Client> GetClient([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Clients.Where(client => client.PrimaryKey == key).Include(s=> s.Scopes));
        }

        // PUT odata/Client(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != client.PrimaryKey)
            {
                return BadRequest();
            }

            db.Entry(client).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(client);
        }

        // POST odata/Client
        public async Task<IHttpActionResult> Post(Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Clients.Add(client);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ClientExists(client.PrimaryKey))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(client);
        }

        // PATCH odata/Client(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<Client> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Client client = await db.Clients.FindAsync(key);
            if (client == null)
            {
                return NotFound();
            }

            patch.Patch(client);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(client);
        }

        // DELETE odata/Client(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            Client client = await db.Clients.FindAsync(key);
            if (client == null)
            {
                return NotFound();
            }

            db.Clients.Remove(client);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/Client(5)/Application
        [Queryable]
        public SingleResult<Application> GetApplication([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Clients.Where(m => m.PrimaryKey == key).Select(m => m.Application));
        }

        // GET odata/Client(5)/Scopes
        [Queryable]
        public IQueryable<Scope> GetScopes([FromODataUri] Guid key)
        {
            return db.Clients.Where(m => m.PrimaryKey == key).SelectMany(m => m.Scopes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClientExists(Guid key)
        {
            return db.Clients.Count(e => e.PrimaryKey == key) > 0;
        }
    }
}
