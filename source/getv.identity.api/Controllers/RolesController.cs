﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IdentityModel.Claims;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using Foundation.WebApi;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Identity.Api.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using Identity.Api;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Role>("Roles");
    builder.EntitySet<Client>("Client"); 
    builder.EntitySet<Application>("Application"); 
    builder.EntitySet<ClaimEntity>("ClaimEntity"); 
    builder.EntitySet<User>("User"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class RolesController : ODataController
    {
        private IdentityContext db = new IdentityContext();

        // GET odata/Roles
        [HttpGet]
        [Queryable]
        [Route("roles")]
        public IQueryable<Role> GetRoles()
        {
            return db.Roles;
        }

        // GET odata/Roles(5)
        [Queryable]
        public SingleResult<Role> GetRole([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Roles.Where(role => role.PrimaryKey == key));
        }

        // PUT odata/Roles(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Role role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != role.PrimaryKey)
            {
                return BadRequest();
            }

            db.Entry(role).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(role);
        }

        //// POST odata/Roles
        //public async Task<IHttpActionResult> Post(Role role)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Roles.Add(role);

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (RoleExists(role.PrimaryKey))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Created(role);
        //}

        [HttpPost]
        [Route("roles")]
        //[ResponseProjection(typeof(ScopeResponse))]
        public IEnumerable<Role> Post(HttpRequestMessage request)
        {
            var json = HttpRequestHelpers.GetJSONFromRequestBody(request);
            if (json == null)
            {
                return null;
            }
            SaveRoles(db, json);
            var result = db.Set<Role>()
                .Where(e => !e.IsRemoved);
            return result;
        }

        private void SaveRoles(IdentityContext ccontext, String json)
        {
            try
            {
                var items = JsonConvert.DeserializeObject<List<dynamic>>(json);
                foreach (var item in items)
                {
                    string appToFind = item.application;
                    var entity = ccontext.Set<Role>().Create();
                    entity.Id = item.id;
                    entity.Users = new List<User>();
                    if (item.users != null)
                    {
                        foreach (string userId in item.users)
                        {
                            // Find the First or only Client
                            var userToAdd = db.Set<User>().FirstOrDefault(t => t.Id == userId);

                            if (userToAdd != null)
                            {
                                entity.Users.Add(userToAdd);
                            }
                            else
                            {
                                throw new Exception(
                                  "Error occured adding a new Scope. Client(s) missing or not found.");
                            }
                        }
                    }
                    if (item.application != null)
                    {
                        Application applicationToAdd = ccontext.Applications.FirstOrDefault(t => t.Id == appToFind);
                        if (applicationToAdd == null)
                        {
                            throw new Exception(
                                "Error occured adding a new Scope. Application missing or not found.");
                        }
                        entity.Application = applicationToAdd;

                    }
                    entity.Description = item.description;
                    entity.Name = item.name;
                    entity.AllowedClients = new List<Client>();
                    if (item.allowedclients != null)
                    {
                        foreach (string client in item.allowedclients)
                        {
                            // Find the First or only Client
                            var clientToAdd = db.Set<Client>().FirstOrDefault(t => t.Id == client);

                            if (clientToAdd != null)
                            {
                                entity.AllowedClients.Add(clientToAdd);
                            }
                            else
                            {
                                throw new Exception(
                                  "Error occured adding a new Scope. Client(s) missing or not found.");
                            }
                        }
                    }

                    // Initialize anything that is not set explicitly above
                    entity.Initialize();

                    // INSERT the new Scope via EF
                    ccontext.Set<Role>().Add(entity);
                }
                ccontext.SaveChanges();
            }
            catch (Exception ex)
            {
                // toss the error out to ELMAH
                throw ex;
            }

            // Uncomment to reveal EF Errors
            //catch (DbEntityValidationException dbEx)
            //{
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            System.Diagnostics.Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
            //                validationErrors.Entry.Entity.GetType().FullName,
            //                validationError.PropertyName,
            //                validationError.ErrorMessage);
            //        }
            //    }

            //    throw;  // You can also choose to handle the exception here...
            //}
        }

        // PATCH odata/Roles(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<Role> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Role role = await db.Roles.FindAsync(key);
            if (role == null)
            {
                return NotFound();
            }

            patch.Patch(role);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoleExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(role);
        }

        // DELETE odata/Roles(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            Role role = await db.Roles.FindAsync(key);
            if (role == null)
            {
                return NotFound();
            }

            db.Roles.Remove(role);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/Roles(5)/AllowedClients
        [Queryable]
        public IQueryable<Client> GetAllowedClients([FromODataUri] Guid key)
        {
            return db.Roles.Where(m => m.PrimaryKey == key).SelectMany(m => m.AllowedClients);
        }

        // GET odata/Roles(5)/Application
        [Queryable]
        public SingleResult<Application> GetApplication([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Roles.Where(m => m.PrimaryKey == key).Select(m => m.Application));
        }

        // GET odata/Roles(5)/Claims
        [Queryable]
        public IQueryable<ClaimEntity> GetClaims([FromODataUri] Guid key)
        {
            return db.Roles.Where(m => m.PrimaryKey == key).SelectMany(m => m.Claims);
        }

        // GET odata/Roles(5)/Users
        [Queryable]
        public IQueryable<User> GetUsers([FromODataUri] Guid key)
        {
            return db.Roles.Where(m => m.PrimaryKey == key).SelectMany(m => m.Users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoleExists(Guid key)
        {
            return db.Roles.Count(e => e.PrimaryKey == key) > 0;
        }
    }
}
