﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.IdentityModel.Claims;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using Foundation.WebApi;
using System.Collections.Generic;
using Newtonsoft.Json;
// using System.Diagnostics;  // required for verbose EF errors

namespace Identity.Api.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using Identity.Api;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Scope>("Scopes");
    builder.EntitySet<Client>("Client"); 
    builder.EntitySet<Application>("Application"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */

    public class ScopesController : ODataController
    {
        private IdentityContext db = new IdentityContext();
        private ClaimsAuthorizeAttribute authorize = new ClaimsAuthorizeAttribute();
        private User user = new User();

        // GET odata/Scopes
        [Queryable]
        [Route("Scopes")]
        public IQueryable<Scope> GetScopes()
        {

            var authtype = ActionContext.RequestContext.Principal.Identity.AuthenticationType;
            var authname = ActionContext.RequestContext.Principal.Identity.Name;
             
            if ( authname != "")
            {
                // send the username to be auth'd

            }
            user.Username = authname;

            Claim readScope = new Claim("Claim","Scopes","Read");

            //var isAllowed = 
            //    authorize.VerifyClaim(ActionContext, readScope) ;
            return db.Scopes;
        }

        // GET odata/Scopes(5)
        [Queryable]
        public SingleResult<Scope> GetScope([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Scopes.Where(scope => scope.PrimaryKey == key));
        }

        // PUT odata/Scopes(5)
        [HttpPut]
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Scope scope)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != scope.PrimaryKey)
            {
                return BadRequest();
            }

            db.Entry(scope).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScopeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(scope);
        }

        // POST odata/Scopes
        //[HttpPost]
        //public async Task<IHttpActionResult> Post(Scope scope)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    db.Scopes.Add(scope);

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (ScopeExists(scope.PrimaryKey))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return Created(scope);
        //}

        [HttpPost]
        [Route("Scopes/GetFiltered")]
        //[ResponseProjection(typeof(ScopeResponse))]
        public IEnumerable<Scope> GetFiltered(HttpRequestMessage request)
        {
            var json = HttpRequestHelpers.GetJSONFromRequestBody(request);
            if (json == null)
            {
                return null;
            }
            var scopeitem = JsonConvert.DeserializeObject<dynamic>(json);

            var result = db.Set<Scope>()
             //   .Where(e => !e.IsRemoved)
             //   .ForEachAsync(scopeitem.clients.Any(c => c.client))
              //  .Where(a => a.Application.Name == (string)scopeitem.app)
                .Take((int) scopeitem.maxrecords);
               // .Contains((string)scopeitem.filter);
            return result;
        }
        

        [HttpPost]
        [Route("Scopes")]
        //[ResponseProjection(typeof(ScopeResponse))]
        public IEnumerable<Scope> Post(HttpRequestMessage request)
        {
            var json = HttpRequestHelpers.GetJSONFromRequestBody(request);
            if (json == null)
            {
                return null;
            }
            SaveScopes(db, json);
            var result = db.Set<Scope>()
                .Where(e => !e.IsRemoved);
            return result;
        }

        private void SaveScopes(IdentityContext ccontext, String json)
        {
            try
            {
                var items = JsonConvert.DeserializeObject<List<dynamic>>(json);
                foreach (var item in items)
                {
                    string appToFind = item.application;
                    var entity = ccontext.Set<Scope>().Create();
                    //entity.Id = item.id;       
                    if (item.application != null)
                    {
                            Application applicationToAdd = ccontext.Applications.FirstOrDefault(t => t.Id == appToFind);
                            if (applicationToAdd == null)
                            {
                                throw new Exception(
                                    "Error occured adding a new Scope. Application missing or not found.");
                            }
                            entity.Application = applicationToAdd;

                    }
                    entity.Description = item.description;
                    entity.Name = item.name; 
                    entity.AllowedClients = new List<Client>();
                    if (item.allowedclients != null)
                    {
                        foreach (string clientId in item.allowedclients)
                        {
                            // Find the First or only Client
                            var clientToAdd = db.Set<Client>().FirstOrDefault(t => t.Id == clientId);

                            if (clientToAdd != null)
                            {
                                entity.AllowedClients.Add((Client)clientToAdd);                              
                            }
                            else
                            {
                                throw new Exception(
                                  "Error occured adding a new Scope. Client(s) missing or not found.");
                            }                           
                        }
                    }

                    // Initialize anything that is not set explicitly above
                    entity.Initialize();
        
                    // INSERT the new Scope via EF
                    ccontext.Set<Scope>().Add(entity);
                }
                ccontext.SaveChanges();
            }
            catch (Exception ex)
            {
                // toss the error out to ELMAH
                throw ex;
            }

            // Uncomment to reveal EF Errors
            //catch (DbEntityValidationException dbEx)
            //{
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            System.Diagnostics.Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
            //                validationErrors.Entry.Entity.GetType().FullName,
            //                validationError.PropertyName,
            //                validationError.ErrorMessage);
            //        }
            //    }

            //    throw;  // You can also choose to handle the exception here...
            //}
        }


        // TODO : REplace this hardcoded preflight 
        [HttpOptions]
        public HttpResponseMessage Options()
        {
            // Temporary message handler   JFE  - re-integrate ThinkTechure
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return response;
        }
  


        // PATCH odata/Scopes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<Scope> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Scope scope = await db.Scopes.FindAsync(key);
            if (scope == null)
            {
                return NotFound();
            }

            patch.Patch(scope);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScopeExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(scope);
        }

        // DELETE odata/Scopes(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            Scope scope = await db.Scopes.FindAsync(key);
            if (scope == null)
            {
                return NotFound();
            }

            db.Scopes.Remove(scope);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/Scopes(5)/AllowedClients
        [Queryable]
        public IQueryable<Client> GetAllowedClients([FromODataUri] Guid key)
        {
            return db.Scopes.Where(m => m.PrimaryKey == key).SelectMany(m => m.AllowedClients);
        }

        // GET odata/Scopes(5)/Application
        [Queryable]
        public SingleResult<Application> GetApplication([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Scopes.Where(m => m.PrimaryKey == key).Select(m => m.Application));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ScopeExists(Guid key)
        {
            return db.Scopes.Count(e => e.PrimaryKey == key) > 0;
        }
    }
}
