﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;

namespace Identity.Api.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using Identity.Api;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<ClaimEntity>("Claims");
    builder.EntitySet<User>("User"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ClaimsController : ODataController
    {
        private IdentityContext db = new IdentityContext();

        // GET odata/Claims
        [Queryable]
        public IQueryable<ClaimEntity> GetClaims()
        {
            return db.ClaimEntities;
        }

        // GET odata/Claims(5)
        [Queryable]
        public SingleResult<ClaimEntity> GetClaimEntity([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.ClaimEntities.Where(claimentity => claimentity.PrimaryKey == key));
        }

        // PUT odata/Claims(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, ClaimEntity claimentity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != claimentity.PrimaryKey)
            {
                return BadRequest();
            }

            db.Entry(claimentity).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClaimEntityExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(claimentity);
        }

        // POST odata/Claims
        public async Task<IHttpActionResult> Post(ClaimEntity claimentity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ClaimEntities.Add(claimentity);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ClaimEntityExists(claimentity.PrimaryKey))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(claimentity);
        }

        // PATCH odata/Claims(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<ClaimEntity> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ClaimEntity claimentity = await db.ClaimEntities.FindAsync(key);
            if (claimentity == null)
            {
                return NotFound();
            }

            patch.Patch(claimentity);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClaimEntityExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(claimentity);
        }

        // DELETE odata/Claims(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            ClaimEntity claimentity = await db.ClaimEntities.FindAsync(key);
            if (claimentity == null)
            {
                return NotFound();
            }

            db.ClaimEntities.Remove(claimentity);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/Claims(5)/Users
        [Queryable]
        public IQueryable<User> GetUsers([FromODataUri] Guid key)
        {
            return db.ClaimEntities.Where(m => m.PrimaryKey == key).SelectMany(m => m.Users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClaimEntityExists(Guid key)
        {
            return db.ClaimEntities.Count(e => e.PrimaryKey == key) > 0;
        }
    }
}
