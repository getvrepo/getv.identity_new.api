﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.OData;
using Foundation.WebApi;
using Newtonsoft.Json;

namespace Identity.Api.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using Identity.Api;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Application>("Applications");
    builder.EntitySet<Client>("Client"); 
    builder.EntitySet<Scope>("Scope"); 
    builder.EntitySet<User>("User"); 
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ApplicationsController : ODataController
    {
        private IdentityContext db = new IdentityContext();

        // GET odata/Applications
        [HttpGet]
        [Queryable]
        [Route("Applications")]
        public IQueryable<Application> GetApplications()
        {
            return db.Applications
                .Include(c=>c.Clients)
                .Include(s=>s.Scopes);
        }

        // GET odata/Applications(5)
        [HttpGet]
        [Queryable]
        [Route("Application")]
        public SingleResult<Application> GetApplication([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Applications.Where(application => application.PrimaryKey == key));
        }

        [HttpPost]
        [Route("Application")]
        public Application FindApplication(HttpRequestMessage request)
        {
            Application appl = new Application();
            try
            {
                var json = HttpRequestHelpers.GetJSONFromRequestBody(request);
                if (json != null)
                {
                    var item = JsonConvert.DeserializeObject<dynamic>(json);
                    string keyword = item.keyword;

                    // only show active Applications, not removed ones
                    bool allowRemoved = false;
                    bool showRemoved = false;
                    if (item.returnonlyactive != null)
                    {
                        if (item.returnonlyactive == true)
                        {
                            allowRemoved = true;
                            showRemoved = false;
                        }
                        {
                            allowRemoved = true;
                            showRemoved = true;
                        }
                    }

                    // TODO continue from here 24 April JFE!!!  Need better boolean filtering
                    appl = db.Applications.FirstOrDefault(
                        u => (u.Name.Contains(keyword)
                            || u.Id.Contains(keyword)
                           
                             //&& u.IsRemoved == allowRemoved 
                             //&& u.IsRemoved == showRemoved
                             )
                             );
                                
                    if (appl == null)
                    {
                        throw new Exception("Application was not found.");
                    }                    
                }            
            }
            catch (Exception ex)
            {
                // need something better here
                throw ex;
            }

            // always return a user or return an exception>?
            return appl;
        }

        // PUT odata/Applications(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Application application)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != application.PrimaryKey)
            {
                return BadRequest();
            }

            db.Entry(application).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(application);
        }

        // POST odata/Applications
        public async Task<IHttpActionResult> Post(Application application)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Applications.Add(application);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ApplicationExists(application.PrimaryKey))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(application);
        }

        // PATCH odata/Applications(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<Application> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Application application = await db.Applications.FindAsync(key);
            if (application == null)
            {
                return NotFound();
            }

            patch.Patch(application);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(application);
        }

        // DELETE odata/Applications(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            Application application = await db.Applications.FindAsync(key);
            if (application == null)
            {
                return NotFound();
            }

            db.Applications.Remove(application);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET odata/Applications(5)/Clients
        [Queryable]
        public IQueryable<Client> GetClients([FromODataUri] Guid key)
        {
            return db.Applications.Where(m => m.PrimaryKey == key).SelectMany(m => m.Clients);
        }

        // GET odata/Applications(5)/Scopes
        [Queryable]
        public IQueryable<Scope> GetScopes([FromODataUri] Guid key)
        {
            return db.Applications.Where(m => m.PrimaryKey == key).SelectMany(m => m.Scopes);
        }

        // GET odata/Applications(5)/Users
        [Queryable]
        public IQueryable<User> GetUsers([FromODataUri] Guid key)
        {
            return db.Applications.Where(m => m.PrimaryKey == key).SelectMany(m => m.Users);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ApplicationExists(Guid key)
        {
            return db.Applications.Count(e => e.PrimaryKey == key) > 0;
        }
    }
}
