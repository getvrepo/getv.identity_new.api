﻿using System;
using System.Collections.Concurrent;
using System.Data.Entity;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;

namespace Identity.Api
{

	public class GetvAccessTokenProvider : IAuthenticationTokenProvider
	{
		private static readonly ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();
//		private AuthenticationTokenProvider tokenProvider;
	//	private readonly DbContext context = new GetvSecurityContext();
        private readonly DbContext context = new IdentityContext();

		private static byte[] GetRandomBytes(int len)
		{
			using (var rng = RandomNumberGenerator.Create())
			{
				var key = new byte[256];
				rng.GetBytes(key);
				return key;
			}
		}
		
		public async Task CreateAsync(AuthenticationTokenCreateContext oauthContext)
		{
			var applicationPrimaryKey = oauthContext.OwinContext.Get<string>("as:app_id");
			if (applicationPrimaryKey == null)
			{
				throw new Exception("Application not found.");
			}

			var application = context.Set<Application>().Find(new Guid(applicationPrimaryKey));
			if (application == null)
			{
				throw new Exception("Application not found.");
			}

			var clientId = oauthContext.OwinContext.Get<string>("as:client_id");
			var client = context.Set<Client>().FirstOrDefault(e => e.ClientId == clientId);
			if (client == null)
			{
				throw new Exception("Client not found.");
			}

			var tokenHandler = new JwtSecurityTokenHandler();

			var signingCredentials = new SigningCredentials(
				new InMemorySymmetricSecurityKey(application.SigningKey),
				"http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
				"http://www.w3.org/2001/04/xmlenc#sha256");

			var now = DateTime.UtcNow;

			var tokenLifetime = (client.AccessTokenLifetime > 0) ? client.AccessTokenLifetime : application.AccessTokenLifetime;

			var token = new JwtSecurityToken(issuer: application.Issuer, audience: application.Audience, claims: oauthContext.Ticket.Identity.Claims, lifetime:
				new Lifetime(now, now.AddMinutes(tokenLifetime)), signingCredentials: signingCredentials);

			var tokenString = tokenHandler.WriteToken(token);

			oauthContext.SetToken(tokenString);
		}

		public async Task ReceiveAsync(AuthenticationTokenReceiveContext oauthContext)
		{
			AuthenticationTicket ticket;
			if (_refreshTokens.TryRemove(oauthContext.Token, out ticket))
			{
				oauthContext.SetTicket(ticket);
			}
		}

		public void Create(AuthenticationTokenCreateContext oauthContext)
		{
			throw new NotImplementedException();
		}

		public void Receive(AuthenticationTokenReceiveContext oauthContext)
		{
			throw new NotImplementedException();
		}
	}
}