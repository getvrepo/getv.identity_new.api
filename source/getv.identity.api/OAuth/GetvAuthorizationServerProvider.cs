﻿using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Claims;
using System.Security.Claims;
using System.Threading.Tasks;
using Foundation.Core;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Claim = System.Security.Claims.Claim;

namespace Identity.Api
{
	public class GetvAuthorizationServerProvider : OAuthAuthorizationServerProvider
	{
		//private readonly DbContext context = new GetvSecurityContext();
        private readonly DbContext context = new IdentityContext();

		/// <summary>
		/// Called when a request to the Token endpoint arrives with a "grant_type" of "client_credentials". This occurs when a registered client
		///             application wishes to acquire an "access_token" to interact with protected resources on it's own behalf, rather than on behalf of an authenticated user. 
		///             If the web application supports the client credentials it may assume the context.ClientId has been validated by the ValidateClientAuthentication call.
		///             To issue an access token the context.Validated must be called with a new ticket containing the claims about the client application which should be associated
		///             with the access token. The application should take appropriate measures to ensure that the endpoint isn’t abused by malicious callers.
		///             The default behavior is to reject this grant type.
		///             See also http://tools.ietf.org/html/rfc6749#section-4.4.2
		/// </summary>
		/// <param name="context">The context of the event carries information in and results out.</param>
		/// <returns>
		/// Task to enable asynchronous execution
		/// </returns>
		public override async Task GrantClientCredentials(OAuthGrantClientCredentialsContext oauthContext)
		{
			var id = new ClaimsIdentity(oauthContext.Options.AuthenticationType);
			id.AddClaim(new Claim("clientId", oauthContext.ClientId));
			oauthContext.OwinContext.Get<List<string>>("as:scope_names").ForEach(s => id.AddClaim(new Claim("scope", s)));
			// create metadata to pass on to refresh token provider
			var props = new AuthenticationProperties(new Dictionary<string, string>
					{
						{ "as:client_id", oauthContext.ClientId }
					});

			var ticket = new AuthenticationTicket(id, props);
			oauthContext.Validated(ticket);
		}


		public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext oauthContext)
		{
			string username, secret;
			if (oauthContext.TryGetBasicCredentials(out username, out secret))
			{

				var client = await context.Set<Client>()
					.Include(e => e.Application)
					.Include(e => e.Scopes)
					.FirstOrDefaultAsync(e => e.ClientId == username);
				if (client == null)
				{
					return;
				}

				var isValidPassword = PasswordHasher.VerifyFormattedPassword(client.ClientSecret, secret);

				if (isValidPassword)
				{
					oauthContext.OwinContext.Set<string>("as:client_id", client.ClientId);
					oauthContext.OwinContext.Set<string>("as:app_id", client.Application.PrimaryKey.ToString());
					var scopeNames = new List<string>();


					client.Scopes.ForEach(s => scopeNames.Add(s.Name));
					oauthContext.OwinContext.Set<List<string>>("as:scope_names", scopeNames);
					oauthContext.Validated();
				}

			}
		}

		public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext oauthContext)
		{
            var user = await context.Set<User>().FirstOrDefaultAsync(e => e.Username == oauthContext.UserName);
			if (user == null)
			{
				oauthContext.Rejected();
				return;
			}
			var isValidPassword = PasswordHasher.VerifyFormattedPassword(user.PasswordHash, oauthContext.Password);

			if (isValidPassword)
			{
				var id = new ClaimsIdentity(oauthContext.Options.AuthenticationType);
				id.AddClaim(new Claim("sub", user.Username));
				id.AddClaim(new Claim("role", "user"));
				id.AddClaim(new Claim("email", user.EmailAddress));
				id.AddClaim(new Claim("givenname", user.FirstName));
				id.AddClaim(new Claim("surname", user.LastName));
				id.AddClaim(new Claim("SomeData", Rights.PossessProperty));
				id.AddClaim(new Claim("clientId", oauthContext.ClientId));
				oauthContext.OwinContext.Get<List<string>>("as:scope_names").ForEach(s => id.AddClaim(new Claim("scope", s)));

               


				// create metadata to pass on to refresh token provider
				var props = new AuthenticationProperties(new Dictionary<string, string>
					{
						{ "as:client_id", oauthContext.ClientId }
					});

				var ticket = new AuthenticationTicket(id, props);
				oauthContext.Validated(ticket);
			}


		}

		public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
		{
			var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
			var currentClient = context.OwinContext.Get<string>("as:client_id");

			// enforce client binding of refresh token
			if (originalClient != currentClient)
			{
				context.Rejected();
				return;
			}

			// chance to change authentication ticket for refresh token requests
			var newId = new ClaimsIdentity(context.Ticket.Identity);
			newId.AddClaim(new Claim("newClaim", "refreshToken"));

			var newTicket = new AuthenticationTicket(newId, context.Ticket.Properties);
			context.Validated(newTicket);
		}
	}
}