﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;

namespace Identity.Api
{
	// sample persistence of refresh tokens
	// this is not production ready!
	public class GetvRefreshTokenProvider : IAuthenticationTokenProvider
	{
		private static ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();
		//private readonly GetvSecurityContext context = new GetvSecurityContext();

		public async Task CreateAsync(AuthenticationTokenCreateContext oauthContext)
		{
			var guid = Guid.NewGuid().ToString();

			// maybe only create a handle the first time, then re-use for same client
			_refreshTokens.TryAdd(guid, oauthContext.Ticket);

			// consider storing only the hash of the handle
			oauthContext.SetToken(guid);
		}

		public async Task ReceiveAsync(AuthenticationTokenReceiveContext oauthContext)
		{
			AuthenticationTicket ticket;
			if (_refreshTokens.TryRemove(oauthContext.Token, out ticket))
			{
				oauthContext.SetTicket(ticket);
			}
		}

		public void Create(AuthenticationTokenCreateContext oauthContext)
		{
			throw new NotImplementedException();
		}

		public void Receive(AuthenticationTokenReceiveContext oauthContext)
		{
			throw new NotImplementedException();
		}
	}
}